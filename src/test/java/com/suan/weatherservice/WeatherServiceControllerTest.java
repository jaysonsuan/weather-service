package com.suan.weatherservice;

import com.google.common.collect.EvictingQueue;
import com.google.common.collect.Queues;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Queue;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {
        WeatherServiceController.class,
        WeatherServiceExceptionHandler.class})
@ContextConfiguration(classes = WeatherServiceControllerTest.TestAppConfig.class)
public class WeatherServiceControllerTest {
    @Autowired
    private Queue<WeatherInfo> latestFiveWeatherInfos;

    @MockBean
    private RestTemplate restTemplate;

    @MockBean
    private WeatherService weatherService;

    @MockBean
    private WeatherDataRepository repository;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        latestFiveWeatherInfos.clear();

        given(weatherService.getCityWeatherInfo("London"))
                .willReturn(new WeatherInfo("2643743", "London", "overcast clouds", "283.04"));
        given(weatherService.getCityWeatherInfo("Prague"))
                .willReturn(new WeatherInfo("3067696", "Prague", "broken clouds", "276.34"));
        given(weatherService.getCityWeatherInfo("San Francisco"))
                .willReturn(new WeatherInfo("5391959", "San Francisco", "clear sky", "276.04"));
    }

    @Test
    public void shouldReturnWeatherInfo() throws Exception {
        mockMvc.perform(get("/weather/current"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].responseId", is("2643743")))
                .andExpect(jsonPath("$[0].location", is("London")))
                .andExpect(jsonPath("$[0].actualWeather", is("overcast clouds")))
                .andExpect(jsonPath("$[0].temperature", is("283.04")))
                .andExpect(jsonPath("$[1].responseId", is("3067696")))
                .andExpect(jsonPath("$[1].location", is("Prague")))
                .andExpect(jsonPath("$[1].actualWeather", is("broken clouds")))
                .andExpect(jsonPath("$[1].temperature", is("276.34")))
                .andExpect(jsonPath("$[2].responseId", is("5391959")))
                .andExpect(jsonPath("$[2].location", is("San Francisco")))
                .andExpect(jsonPath("$[2].actualWeather", is("clear sky")))
                .andExpect(jsonPath("$[2].temperature", is("276.04")));
    }

    @Test
    public void shouldReturnErrorMessage_failedRestCall() throws Exception {
        given(weatherService.getCityWeatherInfo(anyString())).willThrow(RestClientException.class);

        mockMvc.perform(get("/weather/current"))
                .andExpect(status().is(503))
                .andExpect(jsonPath("$.message", is("Weather data not available")));
    }

    @Test
    public void shouldReturnSavedWeatherInfo_queueNotEmpty() throws Exception {
        List<WeatherInfo> unsavedWeatherInfos = newArrayList(
                new WeatherInfo("2643743", "London", "overcast clouds", "283.04"),
                new WeatherInfo("3067696", "Prague", "broken clouds", "276.34"),
                new WeatherInfo("5391959", "San Francisco", "clear sky", "276.04"));
        List<WeatherInfo> savedWeatherInfos = newArrayList(
                new WeatherInfo(1L, "2643743", "London", "overcast clouds", "283.04", LocalDateTime.of(2018, 12, 30, 9, 17, 0)),
                new WeatherInfo(2L, "3067696", "Prague", "broken clouds", "276.34", LocalDateTime.of(2018, 12, 30, 9, 17, 1)),
                new WeatherInfo(3L, "5391959", "San Francisco", "clear sky", "276.04", LocalDateTime.of(2018, 12, 30, 9, 17, 2)));
        latestFiveWeatherInfos.addAll(unsavedWeatherInfos);
        given(repository.saveAll(latestFiveWeatherInfos)).willReturn(savedWeatherInfos);

        mockMvc.perform(post("/weather/save"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].responseId", is("2643743")))
                .andExpect(jsonPath("$[0].location", is("London")))
                .andExpect(jsonPath("$[0].actualWeather", is("overcast clouds")))
                .andExpect(jsonPath("$[0].temperature", is("283.04")))
                .andExpect(jsonPath("$[0].dtimeInserted", is("2018-12-30 09:17:00")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].responseId", is("3067696")))
                .andExpect(jsonPath("$[1].location", is("Prague")))
                .andExpect(jsonPath("$[1].actualWeather", is("broken clouds")))
                .andExpect(jsonPath("$[1].temperature", is("276.34")))
                .andExpect(jsonPath("$[1].dtimeInserted", is("2018-12-30 09:17:01")))
                .andExpect(jsonPath("$[2].id", is(3)))
                .andExpect(jsonPath("$[2].responseId", is("5391959")))
                .andExpect(jsonPath("$[2].location", is("San Francisco")))
                .andExpect(jsonPath("$[2].actualWeather", is("clear sky")))
                .andExpect(jsonPath("$[2].temperature", is("276.04")))
                .andExpect(jsonPath("$[2].dtimeInserted", is("2018-12-30 09:17:02")));
    }

    @Test
    public void shouldReturnSavedWeatherInfo_queueEmpty() throws Exception {
        List<WeatherInfo> unsavedWeatherInfos = newArrayList(
                new WeatherInfo("2643743", "London", "overcast clouds", "283.04"),
                new WeatherInfo("3067696", "Prague", "broken clouds", "276.34"),
                new WeatherInfo("5391959", "San Francisco", "clear sky", "276.04"));
        List<WeatherInfo> savedWeatherInfos = newArrayList(
                new WeatherInfo(1L, "2643743", "London", "overcast clouds", "283.04", LocalDateTime.of(2018, 12, 30, 9, 17, 0)),
                new WeatherInfo(2L, "3067696", "Prague", "broken clouds", "276.34", LocalDateTime.of(2018, 12, 30, 9, 17, 1)),
                new WeatherInfo(3L, "5391959", "San Francisco", "clear sky", "276.04", LocalDateTime.of(2018, 12, 30, 9, 17, 2)));
        latestFiveWeatherInfos.addAll(unsavedWeatherInfos);
        given(repository.saveAll(latestFiveWeatherInfos)).willReturn(savedWeatherInfos);

        mockMvc.perform(post("/weather/save"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @TestConfiguration
    static class TestAppConfig {
        @Bean
        public Queue<WeatherInfo> latestFiveWeatherInfos() {
            return Queues.synchronizedQueue(EvictingQueue.create(5));
        }
    }
}
