package com.suan.weatherservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiError {
    private String message;

    @JsonCreator
    public ApiError(@JsonProperty String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
