package com.suan.weatherservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class WeatherServiceExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherServiceExceptionHandler.class);

    @ExceptionHandler(WeatherServiceException.class)
    public ResponseEntity<ApiError> handleWeatherServiceException(WeatherServiceException ex) {
        LOGGER.error("Error calling OpenWeather service", ex);
        return ResponseEntity.status(503).body(new ApiError("Weather data not available"));
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ApiError> handleUncaughtExceptions(Throwable ex) {
        LOGGER.error("Uncaught exception", ex);
        return ResponseEntity.status(500).body(new ApiError("Internal Server Error"));
    }
}
