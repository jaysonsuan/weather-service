package com.suan.weatherservice;

import com.google.common.collect.EvictingQueue;
import com.google.common.collect.Queues;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.repository.config.EnableJdbcAuditing;
import org.springframework.web.client.RestTemplate;

import java.util.Queue;

@Configuration
@EnableJdbcAuditing
public class AppConfig {
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public Queue<WeatherInfo> latestFiveWeatherInfos() {
        return Queues.synchronizedQueue(EvictingQueue.create(5));
    }
}
