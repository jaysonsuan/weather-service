package com.suan.weatherservice;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OpenWeatherResponse {
    @JsonProperty
    private String id;

    @JsonProperty("name")
    private String city;

    @JsonProperty("weather")
    private List<Weather> weathers;

    @JsonProperty("main")
    private Temperature temperature;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<Weather> getWeathers() {
        return weathers;
    }

    public void setWeathers(List<Weather> weathers) {
        this.weathers = weathers;
    }

    public String  getTemperature() {
        return temperature.get();
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return String.format("OpenWeatherResponse={id=%s, city=%s, weathers=%s, temperature=%s}",
                id, city, weathers, temperature);
    }
}

class Weather {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("Weather={description=%s}", description);
    }
}

class Temperature {
    @JsonProperty("temp")
    private String temperature;

    public String get() {
        return temperature;
    }

    public void set(String temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "Temperature=" + temperature;
    }
}
