package com.suan.weatherservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/weather")
public class WeatherServiceController {
    @Autowired
    private WeatherService weatherService;

    @Autowired
    private WeatherDataRepository repository;

    @Autowired
    private Queue<WeatherInfo> weatherInfoQueue;

    @GetMapping("/current")
    public List<WeatherInfo> currentWeather() {
        List<String> cities = asList("London", "Prague", "San Francisco");
        try {
            List<CompletableFuture<WeatherInfo>> cityWeatherFutures = cities.stream()
                    .map(city -> CompletableFuture.supplyAsync(() -> weatherService.getCityWeatherInfo(city)))
                    .collect(toList());
            List<WeatherInfo> weatherInfos = cityWeatherFutures.stream()
                    .map(CompletableFuture::join)
                    .collect(toList());
            weatherInfoQueue.addAll(weatherInfos);
            return weatherInfos;
        } catch (CompletionException e) {
            throw new WeatherServiceException("Weather service call not completed", e);
        }
    }

    @PostMapping("/save")
    public Iterable<WeatherInfo> saveWeatherData() {
        Iterable<WeatherInfo> savedWeatherData = repository.saveAll(weatherInfoQueue);
        weatherInfoQueue.clear();
        return savedWeatherData;
    }
}
