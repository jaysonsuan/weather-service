package com.suan.weatherservice;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("WeatherLog")
public class WeatherInfo {
    @Id
    private Long id;

    @Column("responseId")
    private String responseId;

    private String location;

    @Column("actualWeather")
    private String actualWeather;

    private String temperature;

    @CreatedDate
    @Column("dtimeInserted")
    private LocalDateTime dtimeInserted;

    @JsonCreator
    public WeatherInfo(
            @JsonProperty String responseId,
            @JsonProperty String location,
            @JsonProperty String actualWeather,
            @JsonProperty String temperature) {
        this(null, responseId, location, actualWeather, temperature, null);
    }

    @JsonCreator
    public WeatherInfo(
            @JsonProperty Long id,
            @JsonProperty String responseId,
            @JsonProperty String location,
            @JsonProperty String actualWeather,
            @JsonProperty String temperature,
            @JsonProperty @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime dtimeInserted) {
        this.id = id;
        this.responseId = responseId;
        this.location = location;
        this.actualWeather = actualWeather;
        this.temperature = temperature;
        this.dtimeInserted = dtimeInserted;
    }

    public Long getId() {
        return id;
    }

    public String getResponseId() {
        return responseId;
    }

    public String getLocation() {
        return location;
    }

    public String getActualWeather() {
        return actualWeather;
    }

    public String getTemperature() {
        return temperature;
    }

    public LocalDateTime getDtimeInserted() {
        return dtimeInserted;
    }

    @Override
    public String toString() {
        return String.format("WeatherInfo={" +
                        "id=%d, " +
                        "responseId=%s, " +
                        "location=%s, " +
                        "actualWeather=%s, " +
                        "temperature=%s}",
                id, responseId, location, actualWeather, temperature);
    }
}
