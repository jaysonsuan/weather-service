package com.suan.weatherservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.stream.Collectors;

@Service
public class WeatherService {
    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherService.class);

    @Value("${openweather.api_key}")
    private String apiKey;

    @Value("${openweather.endpoint}")
    private String apiEndpoint;

    @Autowired
    private RestTemplate restTemplate;

    public WeatherInfo getCityWeatherInfo(String city) {
        OpenWeatherResponse response = restTemplate.getForObject(
                constructRequestUri(city),
                OpenWeatherResponse.class);
        LOGGER.debug("OpenWeather response: {}", response);
        return new WeatherInfo(
                response.getId(),
                response.getCity(),
                response.getWeathers().stream()
                        .map(Weather::getDescription)
                        .collect(Collectors.joining(",")),
                response.getTemperature());
    }

    private URI constructRequestUri(String city) {
        return UriComponentsBuilder.fromHttpUrl(apiEndpoint)
                .queryParam("q", city)
                .queryParam("APPID", apiKey)
                .build().toUri();
    }
}
