package com.suan.weatherservice;

import org.springframework.data.repository.CrudRepository;

interface WeatherDataRepository extends CrudRepository<WeatherInfo, Long> {
}
