create table WeatherLog(
  id bigserial primary key,
  responseId varchar(25),
  location varchar(15),
  actualWeather varchar(50),
  temperature varchar(10)
  dtimeInserted timestamp
)